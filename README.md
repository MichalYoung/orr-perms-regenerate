# README #

Regenerate Oregon Randonneurs permanents page.  This repository thinks it is a sibling of a directory called "permanents", where it finds and then replaces "permanents.html". 


### Running ###

'regen.sh' is the shell script. Let Michal Young know if it should be 
modified to make different assumptions about where it lives and how it 
is used, or if a more robust version that could live elsewhere should be 
produced. 

This version is for Python version 3.3.  A version for Python 3.4 or greater 
is a one-line change, but I don't know how to make one version work for both. 
 
### Who do I talk to? ###

* Michal Young wrote the scripts and can change them (michal.young@gmail.com)
* Lynne Fitzsimmons designed the page look-and-feel
* Michael Rasmussen is webmaster in charge of ORR site and system administration